#!/bin/bash
set -e

#
# runs TBSS
#
# created: 23.04.2019
# Last edited: 08.08.2019
# JPeixoto | joao.peixoto@psy.ox.ac.uk					
#

# list of variables to be set via the options
basedir="";
TBSS_dir=""; # subject codes - all subject codes, ordered, one subject per row
index_dir=""; # subject list

Usage() {
	echo ""
	echo "Usage: ./TBSS -i=<subject folder> -o=<output directory> -f=<folder list>"
	echo ""
	echo "Required arguments:"
    echo "	-i <folder>	: full path to where your dicom files are stores"
    echo " 	-o <folder>	: full path to where your output will be stored"
    echo "	-f <file>	: full path to the list of subjects to include in TBSS"
    echo ""
    echo "Optional arguments:"
    echo "	-h 		: prompts this usage"
    echo ""
    echo "$(basename "$0"):	preprocesses DWI images."
    echo "Runs all steps of TBSS expect for hypothesis testing - that can be done manually"
    echo "with the correct design matrix - check last line of script for more information."
}

get_opt() {
    arg=`echo $1 | sed 's/=.*//'`; echo $arg
}

get_arg() {
    if [ X`echo $1 | grep '='` = X ] ; then echo "Option $1 requires an argument" 1>&2; exit 1
    else 
	arg=`echo $1 | sed 's/.*=//'`
	if [ X$arg = X ] ; then
	    echo "Option $1 requires an argument" 1>&2; exit 1
	fi
	echo $arg
    fi
}

# Parse them baby
if [ $# -lt 2 ] ; then Usage; exit 0; fi
while [ $# -ge 1 ] ; do
    iarg=`get_opt $1`;
    case "$iarg"
	in
	-i)
	    basedir=`get_arg $1`;
	    # check if user input directory exists
		if [ ! -d $basedir ]; then echo "ERROR: input directory doesn't exist"; exit; fi
	    shift;;
	-f)
	    index_dir=`get_arg $1`;
	    # check if user input directory exists
		if [ ! -f $basedir ]; then echo "ERROR: folder list doesn't exist"; exit; fi
	    shift;;
	-o)
	    TBSS_dir=`get_arg $1`;
	    shift;;
	-h)
	    Usage;
	    exit 0;;
	*)
	    #if [ `echo $1 | sed 's/^\(.\).*/\1/'` = "-" ] ; then 
	    echo "Unrecognised option $1" 1>&2
	    exit 1
	    #fi
	    #shift;;
    esac
done


# TBSS
mkdir -p ${TBSS_dir}/TBSS_FA

for i in `cat ${index_dir}`;
do 
#copy files into TBSS_FA folder to run scripts on 
cp ${basedir}/derivatives/${i}/FDT/dti_FA.nii.gz ${TBSS_dir}/TBSS_FA/${i}_dti_FA.nii.gz
done 

cd ${TBSS_dir}/TBSS_FA

echo "preprocessing tbss"
tbss_1_preproc *.nii.gz

cd ${TBSS_dir}/TBSS_FA
echo "registration tbss"
tbss_2_reg -T

echo "post-registration tbss"
tbss_3_postreg -S

cd ${TBSS_dir}/TBSS_FA/
echo "pre-stats tbss"
tbss_4_prestats 0.3

#now run stats - for example:
#randomise -i all_FA_skeletonised -o tbss -m mean_FA_skeleton_mask -d design.mat -t design.con -n 500 --T2 -V
#(after generating design.mat and design.con)

#Display stats image
#fsleyes -std1mm mean_FA_skeleton -cm green -dr .3 .7 tbss_tstat1 -cm red-yellow -dr 1.5 3 tbss_tfce_tstat1 -cm blue-lightblue -dr 0.949 1 &