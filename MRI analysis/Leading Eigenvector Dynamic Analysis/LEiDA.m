%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LEADING EIGENVECTOR DYNAMICS ANALYSIS (LEiDA)
%
% This function processes, clusters and analyses BOLD data using LEiDA
% Comparing between 2 groups
%
% Note: Step 4 (visualization) can be run independently once
%       LEiDA_results.mat has been saved:
% Choose mode = 'run' or 'read' to run LEiDA, or read saved results
%
% 1 - Read the BOLD data from the folder and computes the BOLD phases
%   - Calculate the instantaneous BOLD synchronization matrix
%   - Compute the Leading Eigenvector at each frame from all fMRI scans
% 2 - Cluster the Leading Eigenvectors into recurrent Functional Networks
% 3 - Compute the probability and lifetimes of each FN in each session
%   - Calculate significance between conditions
%   - Saves the Eigenvectors, Clusters and statistics into LEiDA_results.mat
%
% 4 - Plots FC states and errorbars for each clustering solution
%   - Adds an asterisk when results are significantly different between
%     groups
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Script by Joao Peixoto, 2019
%
% Method first used in
% Cabral, et al. 2017 Scientific reports 7, no. 1 (2017): 5135.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% 1 - Compute the Leading Eigenvectors from the BOLD datasets
    
    disp('Processing the eigenvectors from BOLD data')
    
    % USER: Define here data parameters:
    % SVD
    %n_Controls = 31;
    %n_Patients = 34;
    % CADASIL
    n_Controls = 9;
    n_Patients = 8;
    N_areas    = 90;
    TR         = 1.64;
    Tmax       = 200;  % Set here the total number of frames in each scan
    
    Index_Controls=1:n_Controls;  
    Index_Patients=n_Controls+1:n_Controls+n_Patients;
    n_Subjects=n_Controls+n_Patients; % Total number of scans
    
    % Preallocate variables to save FC patterns and associated information
    V1_all   = zeros((Tmax-2)*n_Subjects,N_areas); % All leading eigenvectors
    t_all=0; % Index of time (starts at 0 and will be updated until n_Sub*(Tmax-2))
    Time_all= zeros((Tmax-2)*n_Subjects,1); % Vector that links each frame to a subject
    
    cd /Users/joao/Documents/Imaging/CADASIL/CADASIL/derivatives/tc_aal
    
    for s=1:n_Subjects
        
        % Adapt to load here the BOLD matrix (NxT) from each subject
        BOLD = load([num2str(s) '.txt']);
       
        % Get the BOLD phase using the Hilbert transform
        Phase_BOLD=zeros(N_areas,Tmax);
        for seed=1:N_areas
            BOLD(seed,:)=BOLD(seed,:)-mean(BOLD(seed,:));
            Phase_BOLD(seed,:) = angle(hilbert(BOLD(seed,:)));
        end
        
        % Slide over time discarding the first and last epochs
        for t=2:Tmax-1
         
            %Calculate the Instantaneous FC (BOLD Phase Synchrony)
            iFC=zeros(N_areas);
            for n=1:N_areas
                for p=1:N_areas
                    iFC(n,p)=cos(Phase_BOLD(n,t)-Phase_BOLD(p,t));
                end
            end
            
            % Get the leading eigenvector
            [V1,~]=eigs(iFC,1);
            if sum(V1)>0
                V1=-V1;
            end
            
            % Save V1 from all frames in all fMRI sessions
            t_all=t_all+1; % Update time
            V1_all(t_all,:)=V1;
            Time_all(t_all)=s;
        end
    end
    clear BOLD tc_aal signal_filt iFC V1 Phase_BOLD
    
    %% 2 - Cluster the Leading Eigenvectors
    
    disp('Clustering the eigenvectors into')
    % Leading_Eig is a matrix containing all the eigenvectors:
    % Collumns: N_areas are brain areas (variables)
    % Rows: (Tmax-2)*n_Subjects are all time points (independent observations)
    
    % Set maximum/minimum number of clusters
    % There is no fixed number of FC states that the brain can display
    % Keep the range small for the first trials
    % Extend the range depending on the hypothesis of each work
    maxk=10;
    mink=5;
    rangeK=mink:maxk;
    
    % Set the parameters for Kmeans clustering
    Kmeans_results=cell(size(rangeK));
    
    for k=1:length(rangeK)
        disp(['- ' num2str(rangeK(k)) ' FC states'])
        [IDX, C, SUMD, D]=kmeans(V1_all,rangeK(k),'Distance','cosine','Replicates',20,'MaxIter',200,'Display','off','Options',statset('UseParallel',1));
        [~, ind_sort]=sort(hist(IDX,1:rangeK(k)),'descend');
        [~,idx_sort]=sort(ind_sort,'ascend');
        Kmeans_results{k}.IDX=idx_sort(IDX);   % Cluster time course - numeric collumn vectors
        Kmeans_results{k}.C=C(ind_sort,:);     % Cluster centroids (FC patterns)
        Kmeans_results{k}.SUMD=SUMD(ind_sort); % Within-cluster sums of point-to-centroid distances
        Kmeans_results{k}.D=D(ind_sort);       % Distance from each point to every centroid end
    end
    
    %% 3 - Analyse the Clustering results
    
    cd /Users/joao/Documents/Imaging/pipelines/rs_pipeline/LEiDA_2GROUPS
    
    % For every fMRI scan calculate probability and lifetimes of each state c.
    P=zeros(n_Subjects,maxk-mink+1,maxk);
    LT=zeros(n_Subjects,maxk-mink+1,maxk);
    
    for k=1:length(rangeK)
        for s=1:n_Subjects
            
            % Select the time points representing this subject and task
            T=(Time_all==s);
            Ctime=Kmeans_results{k}.IDX(T);
            
            for c=1:rangeK(k)
                % Probability
                P(s,k,c)=mean(Ctime==c);
                
                % Mean Lifetime
                Ctime_bin=Ctime==c;
                
                % Detect switches in and out of this state
                a=find(diff(Ctime_bin)==1);
                b=find(diff(Ctime_bin)==-1);
                
                % We discard the cases where state starts or ends ON
                if length(b)>length(a)
                    b(1)=[];
                elseif length(a)>length(b)
                    a(end)=[];
                elseif  ~isempty(a) && ~isempty(b) && a(1)>b(1)
                    b(1)=[];
                    a(end)=[];
                end
                if ~isempty(a) && ~isempty(b)
                    C_Durations=b-a;
                else
                    C_Durations=0;
                end
                LT(s,k,c)=mean(C_Durations)*TR;
            end
        end
    end
    
    P_pval=zeros(maxk-mink+1,maxk);
    
    disp('Testing differences in State Prob between Patients and Controls')
    
    for k=1:length(rangeK)
        disp(['Now running statistics for ' num2str(rangeK(k)) ' FC states'])
        for c=1:rangeK(k)
            % Compare Probabilities
            a=squeeze(P(Index_Controls,k,c))';  % Vector containing Prob of c in Controls
            b=squeeze(P(Index_Patients,k,c))';  % Vector containing Prob of c in Patients
            stats=permutation_htest2_np([a,b],[ones(1,numel(a)) 2*ones(1,numel(b))],1000,0.05,'ttest');
            P_pval(k,c)=min(stats.pvals);
        end
    end
    
    LT_pval=zeros(maxk-mink+1,maxk);
    
    disp('Testing differences in State Lifetimes between Patients and Controls')
    
    for k=1:length(rangeK)
        disp(['Now running statistics for ' num2str(rangeK(k)) ' FC states'])
        for c=1:rangeK(k)
            % Compare Probabilities
            a=squeeze(LT(Index_Controls,k,c))';  % Vector containing Prob of c in Controls
            b=squeeze(LT(Index_Patients,k,c))';  % Vector containing Prob of c in Patients
            stats=permutation_htest2_np([a,b],[ones(1,numel(a)) 2*ones(1,numel(b))],1000,0.05,'ttest');
            LT_pval(k,c)=min(stats.pvals);
        end
    end
    
    disp('%%%%% LEiDA SUCCESSFULLY COMPLETED %%%%%%%')
    disp('LEiDA results are now saved.')
    
    % Before correction:
    [k,c]=ind2sub([length(rangeK),max(rangeK)],find(P_pval<0.05 & P_pval>0));
    disp('Note: Significant differences are detected with:')
    [~,b]=sort(k);
    for h=b'
        disp(['K=' num2str(rangeK(k(h))) ' C=' num2str(c(h)) ' (p=' num2str(P_pval(k(h),c(h))) ')'])
    end
    
    % Bonferroni correction
    repclustmat=repmat(rangeK',1,maxk);
    P_pval_bnf=P_pval.*repclustmat;
    find(P_pval_bnf>0 & P_pval_bnf<0.05);
    
    disp('After correction by 0.05/k significant differences are detected with:')
    [~,b]=sort(k);
    for h=b'
        disp(['K=' num2str(rangeK(k(h))) ' C=' num2str(c(h)) ' (p=' num2str(P_pval_bnf(k(h),c(h))) ')'])
    end
    
    %Plot_p_values.m
    
    disp('')
    disp('Note: Now you can run the following parts independently')
    disp('I.e. without re-running the clustering ')
    disp('Just call: LEiDA_COMPARE2GROUPS(''read'')')
    
    save LEiDA_results.mat V1_all Time_all Kmeans_results P LT P_pval P_pval_bnf LT_pval rangeK n_Controls n_Patients N_areas Index_Controls Index_Patients
    
%% 4 - Plot FC patterns and stastistics between groups
cd /Users/joao/Documents/Imaging/pipelines/rs_pipeline/LEiDA_2GROUPS
addpath(genpath('/Applications/MATLAB/Add-Ons'))
disp(' ')
disp('%%% PLOTS %%%%')
disp(['Choose number of clusters between ' num2str(rangeK(1)) ' and ' num2str(rangeK(end)) ])

K = input('Number of clusters: ');
Best_Clusters=Kmeans_results{rangeK==K};
k=find(rangeK==K);

% Get the K patterns
V=Best_Clusters.C;

figure()
colormap(jet)
% Pannel A - Plot the FC patterns over the cortex
% Pannel B - Plot the FC patterns in matrix format
% Pannel C - Plot the probability of each state in each condition
Order=[1:2:N_areas N_areas:-2:2];

for c=1:K
    
    subplot(6,K,c)
    % This needs function plot_nodes_in_cortex.m and aal_cog.m
    plot_nodes_in_cortex(V(c,:))
    title({['State #' num2str(c)]})
    
    subplot(6,K,K+c)
    FC_V=V(c,Order)'*V(c,Order);
    li=max(abs(FC_V(:)));
    imagesc(FC_V,[-li li])
    axis square
    title('FC pattern')
    ylabel('Brain area #')
    xlabel('Brain area #')
    
    Vc=V(c,Order);
    Vc=Vc/max(abs(Vc));
    subplot(6,K,[2*K+c 3*K+c 4*K+c])
    hold on
    barh(Vc.*(Vc<=0),'FaceColor',[0.2  .2  1],'EdgeColor','none','Barwidth',.5)
    barh(Vc.*(Vc>0),'FaceColor',[1 .2 .2],'EdgeColor','none','Barwidth',.5)
    ylim([0 91])
    xlim([-1 1])
    grid on
    set(gca,'YTick',1:N_areas,'Fontsize',8)
    set(gca,'YTickLabel',[])
    title({['PC State ' num2str(c)],['V_C_' num2str(c)]})
    
    subplot(6,K,5*K+c)
    Controls=squeeze(P(Index_Controls,k,c));
    Patients=squeeze(P(Index_Patients,k,c));
    bar([mean(Controls) mean(Patients)],'EdgeColor','w','FaceColor',[.5 .5 .5])
    hold on
    % Error bar containing the standard error of the mean
    errorbar([mean(Controls) mean(Patients)],[std(Controls)/sqrt(numel(Controls)) std(Patients)/sqrt(numel(Patients))],'LineStyle','none','Color','k')
    set(gca,'XTickLabel',{'Ap', '~Ap'})
    if P_pval(k,c)<0.05
        plot(1.4,max([mean(Controls) mean(Patients)])+.01,'*k')
        if P_pval_bnf(k,c)<0.05/K
            plot(1.6,max([mean(Controls) mean(Patients)])+.01,'*k')
        end
    end
    if c==1
        ylabel('Probability')
    end
    box off
end

%% Manual figure
cd /Users/joao/Documents/Imaging/pipelines/rs_pipeline/LEiDA_2GROUPS
addpath(genpath('/Applications/MATLAB/Add-Ons'))
K = 16;
c = 4;
Best_Clusters=Kmeans_results{rangeK==K};
k=find(rangeK==K);
% Get the K patterns
V=Best_Clusters.C;

figure('Renderer', 'painters', 'Position', [500 500 1500 2000])
colormap(jet)
% Pannel A - Plot the FC patterns over the cortex
% Pannel B - Plot the FC patterns in matrix format
% Pannel C - Plot the probability of each state in each condition
Order=[1:2:N_areas N_areas:-2:2];
 
subplot(2,2,1)
% This needs function plot_nodes_in_cortex.m and aal_cog.m
plot_nodes_in_cortex(V(c,:))
title({['State #' num2str(c)]},'Fontsize',20)
    
subplot(2,2,2)
FC_V=V(c,Order)'*V(c,Order);
li=max(abs(FC_V(:)));
imagesc(FC_V,[-li li]) 
axis square
title('FC pattern','Fontsize',20)
ylabel('Brain area #','Fontsize',18)
xlabel('Brain area #','Fontsize',18)
    
Vc=V(c,Order);
Vc=Vc/max(abs(Vc));
subplot(2,2,3)
hold on
barh(Vc.*(Vc<=0),'FaceColor',[0.2  .2  1],'EdgeColor','none','Barwidth',.5)
barh(Vc.*(Vc>0),'FaceColor',[1 .2 .2],'EdgeColor','none','Barwidth',.5)
ylim([0 91])
xlim([-1 1])
grid on
set(gca,'YTick',1:N_areas,'Fontsize',18)
set(gca,'YTickLabel',[])
title({['PC State ' num2str(c)],['V_C_' num2str(c)]})
    
subplot(2,2,4)
Controls=squeeze(P(Index_Controls,k,c));
Patients=squeeze(P(Index_Patients,k,c));
bar([mean(Controls) mean(Patients)],'EdgeColor','w','FaceColor',[.5 .5 .5])
hold on
% Error bar containing the standard error of the mean
errorbar([mean(Controls) mean(Patients)],[std(Controls)/sqrt(numel(Controls)) std(Patients)/sqrt(numel(Patients))],'LineStyle','none','Color','k')
set(gca,'XTickLabel',{'Apathetic', 'Non apathetic'},'Fontsize',18)
if P_pval(k,c)<0.05
    plot(1.4,max([mean(Controls) mean(Patients)])+.01,'*k')
    if P_pval_bnf(k,c)<0.05/K
        plot(1.6,max([mean(Controls) mean(Patients)])+.01,'*k')
    end
end
if c==1
    ylabel('Probability','Fontsize',20)
end
box off