
load LEiDA_results.mat P_pval LT_pval mink maxk
rangeK=mink:maxk;

sigC=zeros(1,length(rangeK));

for k=1:length(rangeK)   
    [Min_p_value(k), sigC(k)]=min(P_pval(k,P_pval(k,:)>0));    
end

figure
semilogy(rangeK,0.05*ones(1,length(rangeK)),'r--')
hold on
semilogy(rangeK,0.05./rangeK,'g--')

for k=1:length(rangeK)   
    semilogy(rangeK(k),P_pval(k,P_pval(k,:)>0),'*k');    
end
semilogy(rangeK,Min_p_value,'*b')
%semilogy(rangeK,Min_p_value.*rangeK,'g*-')

ylabel('Prob Patients vs Controls (p-value)')
xlabel('Number of clusters K')
box off

% LIFETIMES

sigC=zeros(length(rangeK));

for k=1:length(rangeK)   
    [Min_p_value(k), sigC(k)]=min(LT_pval(k,LT_pval(k,:)>0));    
end

figure
semilogy(rangeK,0.05*ones(1,length(rangeK)),'r--')
hold on
semilogy(rangeK,0.05./rangeK,'g--')

for k=1:length(rangeK)   
    semilogy(rangeK(k),LT_pval(k,LT_pval(k,:)>0),'*k');    
end
semilogy(rangeK,Min_p_value,'*b')
%semilogy(rangeK,Min_p_value.*rangeK,'g*-')

ylabel('LT Patients vs Controls (p-value)')
xlabel('Number of clusters K')
box off

figure
load AAL_labels.mat label90
load LEiDA_results.mat Kmeans_results
N_areas=90;
Order=[1:2:N_areas N_areas:-2:2];
label90=label90(Order,:);

V_all=zeros(N_areas,length(rangeK));

for k=1:length(rangeK)
    c=sigC(k);
    V=Kmeans_results{rangeK(k)}.C(c,:);
    V=V(Order);
    V_all(:,k)=V;
    
    subplot(1,length(rangeK),k)
    hold on
    barh(find(V<0),V(V<0),'FaceColor',[0.2  .2  1],'EdgeColor','none','Barwidth',.5)
    barh(find(V>=0),V(V>=0),'FaceColor',[1 .2 .2],'EdgeColor','none','Barwidth',.5)
    ylim([0 91])
    xlim([-.15 .15])
    
    grid on
    set(gca,'YTick',1:N_areas,'Fontsize',8)    
    if k==1
        set(gca,'YTickLabel',label90(end:-1:1,:))
    else
        set(gca,'YTickLabel',[])
    end
    title({['K=' num2str(rangeK(k))],['p=' num2str(Min_p_value(k))]},'Fontsize',10)
end

V_mean=mean(V_all(:,Min_p_value<0.05),2)';

figure
subplot(3,2,1)
plot_nodes_in_cortex(V_mean)
rotate3d
view(0,0)

subplot(3,2,3)
plot_nodes_in_cortex(V_mean)
view(-90,0)
rotate3d

subplot(3,2,[2 4 6])
hold on
barh(find(V_mean<0),V_mean(V_mean<0),'FaceColor',[.5 .5 .5],'EdgeColor','none')
barh(find(V_mean>=0),V_mean(V_mean>=0),'FaceColor','red','EdgeColor','none','Barwidth',.4)
set(gca,'YTick',1:90,'Fontsize',7)
set(gca,'YTickLabel',label90(end:-1:1,:))
ylim([0 91])
xlim([-.15 .15])
title('relative BOLD phase','Fontsize',12)
grid on

subplot(3,2,5)
colormap(jet)
FC_V=V_mean'*V_mean;  
li=max(abs(FC_V(:)));
imagesc(FC_V,[-li li])
axis square
title('FC pattern') 
ylabel('Brain area #')
xlabel('Brain area #')  